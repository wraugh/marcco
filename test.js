/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const snippin = require('snippin')
const tap = require('tap')

// #snip "readme"
// Marcco turns your source code into markdown: comments become the main body
// of the text, and code becomes code blocks inside that text. This lets you
// generate READMEs from your source code, or program in a more literate style.
// Marcco is inspired by [Docco](http://ashkenas.com/docco/).
//
// Behaviour
// ---------
//
// Marcco is language-agnostic; it doesn't try to parse or interpret the source
// code. It just reads it line by line and decides, based on whether the
// `commentMarker` regex matches, whether the line is _prose_ or _code_. Comment
// markers are removed from blocks of prose, whereas indentation is added to
// blocks of code (or code fences, depending on the `codePrefix` option).
//
// So
//
//     #snip "src 1"
//     1 + 1 == 2 // This is some code
//     // Now this is some prose
//     #snip
//
// renders as
//
//     #snip "md 1"
//         1 + 1 == 2 // This is some code
//
//     Now this is some prose
//     #snip
//
// ### Source code is handled line-by-line
//
// Marcco isn't smart enough to handle comments that "open" and "close", because
// it only looks at a single line to decide whether it's a comment or some code.
// For example,
//
//     #snip "src 2"
//     1 + 1 == 2 // This is some code
//     /*
//      * This is still code
//      */
//     // Now this is some prose
//     #snip
//
// becomes
//
//     #snip "md 2"
//         1 + 1 == 2 // This is some code
//         /*
//          * This is still code
//          */
//
//     Now this is some prose
//     #snip
//
// You can use this to your advantage when you want to control which comments
// become paragraphs of prose vs which should remain in the source code:
//
//     #snip "src 3"
//     while (true) {
//         // do something
//     }
//     #snip
//
// would render as the confusing
//
//     #snip "md 3"
//         while (true) {
//
//     do something
//
//         }
//     #snip
//
// so instead you could write
//
//     #snip "src 4"
//     // This is easier to read
//     while (true) {
//         /* do something */
//     }
//     #snip
//
// to get
//
//     #snip "md 4"
//     This is easier to read
//
//         while (true) {
//             /* do something */
//         }
//     #snip
//
// ### Blocks
//
// Marcco groups consecutive comment lines into a single prose block, and
// consecutive lines of code into a single code block. When switching from one
// type of block to another, it adds a newline:
//
//     #snip "src 5"
//     Code 1
//     // Prose A
//     Code 2
//     Code 3
//     // Prose B
//     // Prose C
//     #snip
//
// becomes
//
//     #snip "md 5"
//         Code 1
//
//     Prose A
//
//         Code 2
//         Code 3
//
//     Prose B
//     Prose C
//     #snip
//
// Empty or blank lines don't cause a block to end, even if they're in the middle
// of a block of prose. They're just output as-is:
//
//     #snip "src 6"
//     // This is prose.
//
//     // This is still the same prose block; notice the lack of extra newline.
//     if (a > max) {
//         max = a
//
//         continue // still same code block
//     }
//     #snip
//
// gets you this markdown:
//
//     #snip "md 6"
//     This is prose.
//
//     This is still the same prose block; notice the lack of extra newline.
//
//         if (a > max) {
//             max = a
//
//             continue // still same code block
//         }
//     #snip
//
// ### Indentation
//
// Marcco preserves the indentation you use in your prose blocks by removing
// only the prefix common to all non-blank lines in the block. So for example
// the following are three different ways to write the same prose block:
//
//     #snip "src 7"
//     // Things to do today
//     //
//     //   - ship
//     //   - ship
//
//     // Oh and don't forget to ship!
//     #snip
//
// or
//
//     #snip "src 8"
//             // Things to do today
//             //
//             //   - ship
//             //   - ship
//
//             // Oh and don't forget to ship!
//     #snip
//
// or
//
//     #snip "src 9"
//     //     Things to do today
//     //
//     //       - ship
//     //       - ship
//
//     //     Oh and don't forget to ship!
//     #snip
//
// all render as
//
//     #snip "md 7"
//     #snip "md 8"
//     #snip "md 9"
//     Things to do today
//
//       - ship
//       - ship
//
//     Oh and don't forget to ship!
//     #snip
//     #snip
//     #snip
//
// ### Custom comment markers
//
// You can set the `commentMarker` option to match the language you're using.
// For instance, you could use `/^\s*(\/\/|#)\s*/` for PHP:
//
//     #snip "src php"
//     // PHP supports
//     /* multiple */
//     # comment styles
//     #snip
//
// to get
//
//     #snip "md php"
//     PHP supports
//
//         /* multiple */
//
//     comment styles
//     #snip
//
// or `/^\s*--\s*/` for SQL:
//
//     #snip "src sql"
//     #snip "src sql highlighted"
//     #snip "src sql wat"
//     -- How many users registered in the last 30 days?
//     SELECT COUNT(*) FROM users
//     WHERE registered_at > CURRENT_DATE - INTERVAL '30 days';
//     #snip
//     #snip
//     #snip
//
// which becomes
//
//     #snip "md sql"
//     How many users registered in the last 30 days?
//
//         SELECT COUNT(*) FROM users
//         WHERE registered_at > CURRENT_DATE - INTERVAL '30 days';
//     #snip
//
// ### Custom code prefixes
//
// To benefit from the syntax highlighting provided by some markdown renderers,
// you can set the `codePrefix` option to a [code fence with an info string](
// https://github.github.com/gfm/#fenced-code-blocks). The above example with
// `codePrefix` set to `'~~~sql'` would render like so:
//
//     #snip "md sql highlighted"
//     How many users registered in the last 30 days?
//
//     ~~~sql
//     SELECT COUNT(*) FROM users
//     WHERE registered_at > CURRENT_DATE - INTERVAL '30 days';
//     ~~~
//     #snip
//
// The rule is this: if `codePrefix` looks like a code fence, optionally with
// an info string, then Marcco will fence your code with it. Otherwise, it will
// prefix every line of code with `codePrefix`. So you could use e.g. `'\t'` if
// you would like your code prefixed with tabs rather than the default four
// spaces. It doesn't make a difference to Markdown. Or you can put any string
// in there, e.g. `'> '`:
//
//     #snip "md sql wat"
//     How many users registered in the last 30 days?
//
//     > SELECT COUNT(*) FROM users
//     > WHERE registered_at > CURRENT_DATE - INTERVAL '30 days';
//     #snip
//
// API
// ---
//
// Marcco gives you two ways to turn your code into Markdown: a function that
// works with strings, and a transform for working with streams.
//
// **code2doc(sourceCode, options = {})**
//
//  - `sourceCode` *String* the source code to convert
//  - `options` *Object*
//    - `commentMarker` *RegExp* lines that match this regex count as comments.
//      **Default**: `/^\s*\/\/\s*/`
//    - `codePrefix` *String* the indentation or code fence to use for sections
//      of code. **Default**: `'    '`
//  - Returns: *String*
//
// Returns the Markdown representation of the given source code.
//
// **new DocTx(options = {}, streamOptions = {})**
//
//  - `options` *Object* same as the options passed to `code2doc`
//  - `streamOptions` *Object* options to pass to the constructor of the parent
//                             class, [stream.Transform](https://nodejs.org/docs/latest/api/stream.html#stream_new_stream_transform_options)
//
// Treats its input as source code, and transforms it to markdown.
//
// ### Example
const { code2doc, DocTx } = require('.')

tap.test('Example: convert a shell script to Markdown', t => {
  const script =
    '# Take a screenshot of a region of your screen\n' +
    'maim -s ~/screenshots/$(date -Is | sed "s/:/-/g").png\n'
  const markdown =
    'Take a screenshot of a region of your screen\n' +
    '\n    maim -s ~/screenshots/$(date -Is | sed "s/:/-/g").png\n'

  /* Convert a string by calling code2doc */
  t.equals(code2doc(script, { commentMarker: /^\s*#\s*/ }), markdown)

  /* Convert a stream by piping it to DocTx */
  const tx = new DocTx({ commentMarker: /^\s*#\s*/ })
  let md = ''
  tx.on('data', chunk => { md += chunk })
  tx.on('end', () => {
    t.equals(md, markdown)
    t.end()
  })
  tx.end(script)
})
// #snip

tap.test('Validate examples', t => {
  const cases = {
    '2': {},
    '3': {},
    '4': {},
    '5': {},
    '6': {},
    '7': {},
    '8': {},
    '9': {},
    'php': { commentMarker: /^\s*(\/\/|#)\s*/ },
    'sql': { commentMarker: /^\s*--\s*/ },
    'sql highlighted': { commentMarker: /^\s*--\s*/, codePrefix: '~~~sql' },
    'sql wat': { commentMarker: /^\s*--\s*/, codePrefix: '> ' }
  }
  snippin().getSnippets(__filename).then(snippets => {
    t.plan(Object.keys(cases).length)
    Object.entries(cases).forEach(([k, opts]) => {
      t.equals(code2doc(snippets[`src ${k}`], opts), snippets[`md ${k}`], k)
    })
  })
})

tap.equals(code2doc('//\n//\n'), '\n', 'Whitespace collapse')
tap.equals(code2doc('// a\n//\t\n// b'), 'a\n\t\nb\n', 'Varying prefixes')
tap.test('Default options for DocTx', t => {
  const script = '// lol'
  const markdown = 'lol\n'
  const tx = new DocTx()
  let md = ''
  tx.on('data', chunk => { md += chunk })
  tx.on('end', () => {
    t.equals(md, markdown)
    t.end()
  })
  tx.end(script)
})
