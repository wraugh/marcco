#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

// This script writes README.md from the doc snippets in the codebase

const fs = require('fs')
const snippin = require('snippin')

const { code2doc } = require('.')

const s = snippin()
const testSnippets = s.getSnippets('./test.js')
const out = fs.createWriteStream('./README.md')

testSnippets.then(snippets => {
  const readme = code2doc(snippets.readme, { codePrefix: '```javascript' })
  out.write(readme)
  out.write(`
Contributing
------------

You're welcome to contribute to this project following the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

All patches must follow [standard style](https://standardjs.com/) and have 100%
test coverage. You can make sure of this by adding

    ./.pre-commit

to your git pre-commit hook.
`)
  out.end()
})
