/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const pumpify = require('pumpify')
const { Transform } = require('readable-stream')
const split2 = require('split2')

const newlineRe = /\r\n|[\n\v\f\r\x85\u2028\u2029]/
const blankLineRe = /^\s*$/

class Marcco {
  constructor (markdownSink, options) {
    this.sink = markdownSink
    this._commentMarker = options.commentMarker || /^\s*\/\/\s*/
    this._codeBlockStart = options.codePrefix || '    '
    const m = /^ {0,3}(```+|~~~+)/.exec(this._codeBlockStart)
    this._codeFence = m ? m[1] : ''

    this._haveContent = false
    this._inCodeSection = false
    this._openCommentSection()
  }

  addLine (line) {
    if (blankLineRe.test(line)) {
      this._inCodeSection ? this._addCodeLine(line) : this._addCommentLine(line)

      return
    }

    if (this._commentMarker.test(line)) {
      if (this._inCodeSection) {
        this._closeCodeSection()
        this._openCommentSection()
      }
      this._addCommentLine(line)
    } else {
      if (!this._inCodeSection) {
        this._closeCommentSection()
        this._openCodeSection()
      }
      this._addCodeLine(line)
    }
    this._haveContent = true
  }

  finish () {
    if (this._inCodeSection) {
      this._closeCodeSection()
    } else {
      this._closeCommentSection()
    }
  }

  _openCodeSection () {
    if (this._haveContent) this.sink('\n')
    if (this._codeFence) this.sink(this._codeBlockStart + '\n')
    this._inCodeSection = true
  }

  _addCodeLine (line) {
    if (!this._codeFence && !blankLineRe.test(line)) {
      line = this._codeBlockStart + line
    }
    this.sink(line + '\n')
  }

  _closeCodeSection () {
    if (this._codeFence) {
      this.sink(this._codeFence + '\n')
    }
    this._inCodeSection = false
  }

  // Comments need some pre-processing before they become markdown text: we need
  // to remove the comment marker at the start of each line. We also want to
  // remove the whitespace between the comment marker and the text; otherwise
  // our markdown sections will be slightly indented (most people put a single
  // space between comment markers and comment text).
  //
  // Now because indentation matters to markdown, we can't just strip the
  // comment marker + all whitespace at the start of lines. We need to find the
  // amount of whitepace common to all lines of the comment block, and remove
  // just that and no more. This means we can't output comment lines one at a
  // time as we read them; we need to buffer them until the comment block ends.
  // Only then can we identify the shortest common prefix and remove it.
  _openCommentSection () {
    if (this._haveContent) this.sink('\n')
    this._commentLines = []
  }

  _addCommentLine (line) {
    this._commentLines.push(line)
  }

  _closeCommentSection () {
    if (!this._haveContent) return

    let shortestPrefix = null

    // Find the shortest prefix
    for (let line of this._commentLines) {
      const m = line.match(this._commentMarker)
      if (m === null || m[0].length === line.length) {
        // Just whitespace; this doesn't compete for shortest prefix.
        continue
      }
      if (shortestPrefix === null) {
        shortestPrefix = m[0]
      } else {
        shortestPrefix = this._getCommonPrefix(shortestPrefix, m[0])
      }
    }

    if (shortestPrefix === null) {
      // The comment block is just whitespace. Collapse it.
      this.sink('\n')
      return
    }

    // Now output the comment lines with prefix removed.
    for (let line of this._commentLines) {
      // Because whitespace lines didn't participate in the shortest
      // prefix search, it's possible that they don't start with it.
      // So for each line, we'll remove the common part of the shortest prefix
      // and the line itself.
      let prefixToRemove = this._getCommonPrefix(shortestPrefix, line)
      if (prefixToRemove.length > 0) {
        line = line.replace(prefixToRemove, '')
      }
      this.sink(line + '\n')
    }
  }

  _getCommonPrefix (shorter, longer) {
    // Arguments may be passed in either order. Let's make sure that`longer` is
    // longer than `shorter`.
    if (longer.length < shorter.length) {
      const tmp = shorter
      shorter = longer
      longer = tmp
    }

    while (shorter.length > 0 && longer.slice(0, shorter.length) !== shorter) {
      shorter = shorter.slice(0, -1)
    }

    return shorter
  }
}

function DocTx (options = {}, streamOptions = {}) {
  const doc = new Marcco(null, options)
  const tx = new Transform(Object.assign({}, streamOptions, {
    objectMode: true,
    transform (line, encoding, callback) {
      doc.addLine(line)
      callback()
    },
    flush (callback) {
      doc.finish()
      callback()
    }
  }))
  doc.sink = line => tx.push(line)

  return pumpify(split2(newlineRe, streamOptions), tx)
}

function code2doc (sourceCode, options = {}) {
  let markdown = ''
  const doc = new Marcco(line => { markdown += line }, options)
  const lines = sourceCode.split(newlineRe)
  if (lines[lines.length - 1] === '') {
    lines.pop() // don't add an extra newline at the end of the script
  }
  lines.map(line => doc.addLine(line))
  doc.finish()

  return markdown
}

module.exports.DocTx = DocTx
module.exports.code2doc = code2doc
